//  Action Creator
export const selectData = data => {
    
    //return an action
    return {
        type: 'DATA',
        payload: data
    };
};
