import { combineReducers } from 'redux';

//  Reducer
const dataReducer = () => {
    return [
        { title: 'Home', content: 'ReactJS Web Development Kit.' },
        { title: 'About', content: 'This is a web kit for ReactJS project up and running on local machine for development and testing purposes.' }
    ];
};
 
const selectedDataReducer = (selectedData = null, action) => {
    if(action.type === 'DATA') {
        return action.payload;
    }
    
    return selectedData;
}

export default combineReducers({
    data: dataReducer,
    selectedData: selectedDataReducer
});
