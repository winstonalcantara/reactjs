import React from 'react';

class About extends React.Component {    
    render() {
        return (
            <main role="main">
                <div className="container">
                    <div className="row" id="main-content">
                        <div className="col-lg-12 content">This is a web kit for ReactJS project up and running on local machine for development and testing purposes.</div>
                    </div>
                </div>
            </main>
        );
    }
};

export default About;
