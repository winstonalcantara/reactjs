import React from 'react';

class Error extends React.Component {    
    render() {
        return (
            <main role="main">
                <div className="container">
                    <div className="row" id="main-content">
                        <div className="content">404 Error</div>
                    </div>
                </div>
            </main>
        );
    }
};

export default Error;
