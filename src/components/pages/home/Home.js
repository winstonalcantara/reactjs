import React from 'react';
import { connect } from 'react-redux';

class Home extends React.Component {    
    render() {
        return (
            <main role="main">
                <div className="container">
                    <div className="row" id="main-content">
                       <div className="col-lg-12 content">ReactJS Web Development Kit</div>
                    </div>
                </div>
            </main>
        );
    }
};

const mapStateToProps = (state) => {    
    //  console.log(state);

    return {
        data: state.selectedData
    };
}

export default connect(mapStateToProps)(Home);
