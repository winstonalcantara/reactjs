import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ScrollToTop from '../helper/ScrollToTop';
import Header from './header/Header';
import Footer from './footer/Footer';
import Home from './pages/home/Home';
import About from './pages/about/About';
import Error from './pages/404/Error'; 

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

class App extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            text: 'ReactJS Web Development Kit'
        }
    };
    
    componentDidMount() {
        
    }
    
    componentDidUpdate() {
    
    }
    
    componentWillUnmount() {
        
    }
    
    render() {
        return (
            <Router>
                <ScrollToTop />
                <Header />
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/about" exact component={About} />
                    <Route path="*" component={Error} />
                </Switch>
                <Footer />
            </Router>
        );
    }
}

export default App;
