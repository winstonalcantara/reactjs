import React from 'react';
import { Link } from 'react-router-dom';

import './Nav.css';

class Nav extends React.Component {    
    constructor(props) {
        super(props);
        
        this.state = {
            logo: "/assets/images/W.png",
            menu: {
                home: "/",
                about: "/about"
            }
        }
    };

    render() {
        return (
            <div className="navbar">                
                <nav className="container navbar-expand-md">
                    <Link to={this.state.menu.home} className="navbar-brand"><img src={this.state.logo} className="img-fluid img-logo" alt="W." /></Link>
                    <div className="navbar-toggler" data-toggle="collapse" data-target="#header-nav-menu" aria-controls="header-nav-menu" aria-expanded="false" aria-label="Toggle navigation">
                        <i className="fas fa-bars"></i>
                    </div>
                    <div className="collapse navbar-collapse" id="header-nav-menu">
                        <ul className="navbar-nav ml-auto header-menu">
                            <li className="nav-item"><Link to={this.state.menu.home} className="nav-link">Home</Link></li>
                            <li className="nav-item"><Link to={this.state.menu.about} className="nav-link">About</Link></li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
};

export default Nav;
