import React from 'react';
import { withRouter } from 'react-router-dom';

//  Scroll to top when component change.
class ScrollToTop extends React.Component {
    componentDidUpdate() {
        window.scrollTo(0, 0);
        return (null);
    }
    
    render() {
        return null;
    }
}

export default withRouter(ScrollToTop);