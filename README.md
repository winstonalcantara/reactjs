# ReactJS Web Development Kit

ReactJS Web Development Kit v1.03


## Getting Started

This is a web kit for ReactJS project, up and running on local machine for development and testing purposes.


### Requirements

What you need to setup and install

```
NPM
* axios
* bootstrap
* jquery
* popper.js
* react-dom
* react-router-dom
* react-scripts
* redux
```


## Built With

* [React 16.8](https://github.com/facebook/react/releases/tag/v16.8.6) - A JavaScript library for building user interfaces.
* [Bootstrap 4.3](https://getbootstrap.com/docs/4.3/getting-started/introduction/) - Toolkit for developing with HTML, CSS, and JS.


## Authors

* **Winston Alcantara** - [winstonalcantara.com](https://www.winstonalcantara.com)
